package grafico;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TpGrafos {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TpGrafos window = new TpGrafos();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TpGrafos() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(250, 250, 240));
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel titulo = new JLabel("");
		titulo.setBounds(140, 24, 439, 100);
		titulo.setIcon(new ImageIcon(TpGrafos.class.getResource("/grafico/Recursos/Titulo.png")));
		frame.getContentPane().add(titulo);
		
		JButton botonAņadirPersona = new JButton();
		adiestrarBoton(botonAņadirPersona);
		botonAņadirPersona.setRolloverIcon(new ImageIcon(TpGrafos.class.getResource("/grafico/Recursos/AņadirPersonaBoton2.png")));
		botonAņadirPersona.setPressedIcon(new ImageIcon(TpGrafos.class.getResource("/grafico/Recursos/AņadirPersonaBoton3.png")));
		botonAņadirPersona.setIcon(new ImageIcon(TpGrafos.class.getResource("/grafico/Recursos/AņadirPersonaBoton.png")));
		botonAņadirPersona.setBounds(50, 150, 180, 85);
		frame.getContentPane().add(botonAņadirPersona);
		
		JButton botonVerGrafo = new JButton();
		botonVerGrafo.setBounds(50, 270, 180, 85);
		adiestrarBoton (botonVerGrafo);
		frame.getContentPane().add(botonVerGrafo);
		botonVerGrafo.setRolloverIcon(new ImageIcon(TpGrafos.class.getResource("/grafico/Recursos/VerGrafoBoton2.png")));
		botonVerGrafo.setPressedIcon(new ImageIcon(TpGrafos.class.getResource("/grafico/Recursos/VerGrafoBoton3.png")));
		botonVerGrafo.setIcon(new ImageIcon(TpGrafos.class.getResource("/grafico/Recursos/VerGrafoBoton.png")));
		
		
		JButton botonVerListas = new JButton();
		botonVerListas.setBounds(50, 390, 180, 85);
		adiestrarBoton (botonVerListas);
		frame.getContentPane().add(botonVerListas);
		botonVerListas.setRolloverIcon(new ImageIcon(TpGrafos.class.getResource("/grafico/Recursos/VerListasBoton2.png")));
		botonVerListas.setPressedIcon(new ImageIcon(TpGrafos.class.getResource("/grafico/Recursos/VerListasBoton3.png")));
		botonVerListas.setIcon(new ImageIcon(TpGrafos.class.getResource("/grafico/Recursos/VerListasBoton.png")));
		
		
		JLabel extraterrestre = new JLabel("");
		extraterrestre.setIcon(new ImageIcon(TpGrafos.class.getResource("/grafico/Recursos/Extraterrestre.png")));
		extraterrestre.setBounds(391, 73, 393, 478);
		frame.getContentPane().add(extraterrestre);
		
		JPanel panel = new JPanel();
		panel.setVisible(false);
		panel.setEnabled(false);
		panel.setBackground(Color.BLACK);
		panel.setBounds(0, 0, 800, 600);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
			
		botonAņadirPersona.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				setMenu(titulo, extraterrestre, botonAņadirPersona, botonVerGrafo, botonVerListas, false);
				panel.setVisible(true);
				panel.setEnabled(true);
			}
		});
		
		JButton btnNewButton = new JButton("Volver");
		btnNewButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				setMenu(titulo, extraterrestre, botonAņadirPersona, botonVerGrafo, botonVerListas, true);
				panel.setVisible(false);
				panel.setEnabled(false);
			}
		});
		btnNewButton.setBounds(49, 25, 89, 23);
		panel.add(btnNewButton);
		
	}	
		
	
	private static void adiestrarBoton(JButton boton)
	{
		boton.setContentAreaFilled(false);
		boton.setBorderPainted(false);
	}
	
	private static void setMenu(JLabel label1, JLabel label2, JButton button1, JButton button2, JButton button3, boolean set)
	{
		setLabel(label1, set);
		setLabel(label2, set);
		setButton(button1, set);
		setButton(button2, set);
		setButton(button3, set);
		
	}
	
	private static void setLabel (JLabel label, boolean set)
	{
		label.setEnabled(set);
		label.setVisible(set);
	}
	
	private static void setButton (JButton button, boolean set)
	{
		button.setEnabled(set);
		button.setVisible(set);
	}
}
