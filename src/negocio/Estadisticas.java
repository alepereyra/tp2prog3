package negocio;

import java.util.ArrayList;

public class Estadisticas 
{

	public static double promedioDeportes(ArrayList<Persona> lista)
	{
		double contador = 0;
		for (Persona persona : lista)
			contador += persona.getDeportes();
		contador = contador / lista.size();
		return contador;
	}
	
	public static double promedioMusica(ArrayList<Persona> lista)
	{
		double contador = 0;
		for (Persona persona : lista)
			contador += persona.getMusica();
		contador = contador / lista.size();
		return contador;
	}
	
	public static double promedioCiencia(ArrayList<Persona> lista)
	{
		double contador = 0;
		for (Persona persona : lista)
			contador += persona.getCiencia();
		contador = contador / lista.size();
		return contador;
	}
	
	public static double promedioEspectaculo(ArrayList<Persona> lista)
	{
		double contador = 0;
		for (Persona persona : lista)
			contador += persona.getEspectaculo();
		contador = contador / lista.size();
		return contador;
	}
	
	public static void imprimirPromedios(ArrayList<Persona> lista)
	{
		System.out.println("Promedios:");
		System.out.println("Deportes: " + promedioDeportes(lista) + ", Ciencia: " + promedioCiencia(lista));
		System.out.println("Musica: " + promedioMusica(lista) + ", Espectaculo: " + promedioEspectaculo(lista));
	}
	
	public static void imprimirListas(ArrayList<Persona> lista1, ArrayList<Persona> lista2) {
		System.out.println("lista 1: ");
		for (Persona persona : lista1)
			System.out.println(persona.toString());
		System.out.println("lista 2: ");
		for (Persona persona : lista2)
			System.out.println(persona.toString());
	}
}
