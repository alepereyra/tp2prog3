package negocio;

import java.util.ArrayList;
import java.util.Random;

import grafos.ArbolGeneradorMinimo;
import grafos.Arista;

public class Main {

	public static void main(String[] args) {
		
		// Construyo el grafo completo de personas.
		GrafoPersonas grafoPersonas = new GrafoPersonas();		
		aņadirPersonas(grafoPersonas);
		
		// Construyo el arbol generador minimo del grafo mediante algoritmo de Kruskal. 
		grafoPersonas.setGrafo(ArbolGeneradorMinimo.algoritmoDeKruskal(grafoPersonas.getGrafo()));
		grafoPersonas.imprimirAristas();
		
		// Elimino la arista mas pesada del arbol
		grafoPersonas.eliminarAristaMasPesada();
		
		// Genero las dos listas resultantes del algoritmo y las muestro por consola.
		ArrayList<Persona> lista1 = new ArrayList<Persona>();
		ArrayList<Persona> lista2 = new ArrayList<Persona>();	
		grafoPersonas.devolverListas(lista1, lista2);		
		Estadisticas.imprimirListas(lista1, lista2);
		Estadisticas.imprimirPromedios(lista1);
		Estadisticas.imprimirPromedios(lista2);
	}

	// Metodo que recibe dos listas y las muestra por consola.


	
	// Metodo que introduce los datos en el grafo.
	static void aņadirPersonas(GrafoPersonas grafoPersonas)
	{
		ArrayList<Persona> agregar = new ArrayList<Persona>();
		
		agregar.add( new Persona( "A", 2, 3, 1, 5) ); //El orden es: deportes, ciencia, musica, espectaculo
		agregar.add( new Persona( "B", 5, 5, 1, 1) );
		agregar.add( new Persona( "C", 5, 5, 1, 1) );
		agregar.add( new Persona( "D", 3, 5, 4, 2) );
		agregar.add( new Persona( "E", 3, 3, 3, 3) );

		
		for(Persona persona : agregar)
		{
			grafoPersonas.aņadirPersona(persona);
		}
	}
	
	static void aņadirPersonasAleatorias (GrafoPersonas grafoPersonas, int cantidad)
	{
		ArrayList<Persona> lista = new ArrayList<Persona>();
		for (int i = 0; i < cantidad; i++)
		{
			lista.add(Persona.generarPersonaAleatoria("" + i));
		}
		for (Persona persona : lista)
			grafoPersonas.aņadirPersona(persona);
	}
	

}
