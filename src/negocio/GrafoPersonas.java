package negocio;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import grafos.Arista;
import grafos.BFS;
import grafos.Grafo;

public class GrafoPersonas 
{
	// La posicion del vertice en el grafo coincide con la posicion de la persona en el arreglo. 
	private Grafo grafo;
	private ArrayList<Persona> personas;
	
	// Constructor. Genera el grafo donde tenemos los vertices y aristas y la lista de personas.
	public GrafoPersonas()
	{
		this.grafo = new Grafo(0);
		this.personas = new ArrayList<Persona>();
	}
	
	// Metodo que a�ade una persona a la lista de personas y un vertice universal al grafo.
	public void a�adirPersona(Persona persona)
	{
		grafo.getVecinos().add(new HashSet<Integer>());
		this.personas.add(persona);
		
		for (int i = 0; i < grafo.tama�o() - 1; i++)
		{
			Persona auxiliar = personas.get(i);
			int similaridad = Persona.similaridad(persona, auxiliar);
			grafo.agregarArista(i, grafo.tama�o() -1, similaridad);
		}
	}	
	
	// Metodo que busca la arista con mayor peso del grafo y la elimina.
	public void eliminarAristaMasPesada()
	{
		Arista mayor = grafo.getAristas().get(0);
		for(Arista arista : grafo.getAristas())
		{
			if(arista.getPeso() > mayor.getPeso())
				mayor = arista;
		}
		grafo.eliminarArista(mayor.getVertice1(), mayor.getVertice2());
	}
	
	// Este m�todo parte de un grafo formado por dos componentes conexas y devuelve dos listas que contienen a los vertices de cada una
	public void devolverListas (ArrayList<Persona> lista1, ArrayList<Persona> lista2)
	{
		Set<Integer> alcanzables = BFS.alcanzables(grafo, 0);
		for(int i : alcanzables)
		{
			lista1.add(personas.get(i));
		}
		for (Persona auxiliar : personas)
		{
			if( ! lista1.contains(auxiliar) )
				lista2.add(auxiliar);
		}
	}
	
	// Metodo que recibe un grafo e imprime sus aristas.
	public void imprimirAristas() {
		for(Arista arista : this.getAristas())
			System.out.println(arista);
	}

	
	
	// Getters y Setters
	public Grafo getGrafo()
	{
		return this.grafo;
	}
	
	public void setGrafo(Grafo grafo)
	{
		this.grafo = grafo;
	}
	
	public ArrayList<Arista> getAristas()
	{
		return grafo.getAristas();
	}
	
}
