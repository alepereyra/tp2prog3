package negocio;

import java.util.Random;

import grafos.Verificadores;

public class Persona 
{

	private String nombre;
	private int deportes;
	private int ciencia;
	private int musica;
	private int espectaculo;
	
	// Constructor de la clase.
	public Persona(String nombre, int deportes, int ciencia, int musica, int espectaculo)
	{
		Verificadores.verificarNumeros(deportes, ciencia, musica, espectaculo);
		this.nombre = nombre;
		this.deportes = deportes;
		this.ciencia = ciencia;
		this.musica = musica;
		this.espectaculo = espectaculo;
	}
	
	// Metodo que recibe dos personas y calcula el indice de similaridad entre ambas.
	public static int similaridad (Persona a, Persona b)
	{
		int ret = 0;
		ret = ( (Math.abs( a.getCiencia() - b.getCiencia() )) + (Math.abs( a.getDeportes() - b.getDeportes() )) + 
				(Math.abs( a.getMusica() - b.getMusica() )) + (Math.abs( a.getEspectaculo() - b.getEspectaculo() )) );
		return ret;
	}
	
	static Persona generarPersonaAleatoria(String nombre)
	{
		Random rand = new Random();

		int a = rand.nextInt(5) +1;
		int b = rand.nextInt(5) +1;
		int c = rand.nextInt(5) +1;
		int d = rand.nextInt(5) +1;
		//System.out.println((a + " " + b + " " + c + " " + d + ""));
		Persona ret = new Persona (nombre, a, b, c, d);		
		
		return ret;
	}
	
	// Getters, setters y toString.
	public int getDeportes()
	{
		return this.deportes;
	}
	
	public int getCiencia()
	{
		return this.ciencia;
	}
	
	public int getEspectaculo()
	{
		return this.espectaculo;
	}
	
	public int getMusica()
	{
		return this.musica;
	}
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public String toString()
	{
		String ret = this.nombre;
		return ret;
	}
	

}
