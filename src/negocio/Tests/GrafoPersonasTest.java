package negocio.Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import negocio.GrafoPersonas;
import negocio.Persona;

public class GrafoPersonasTest 
{
	@Test
	public void agregarPrimerPersonaTest() 
	{
		GrafoPersonas grafo = new GrafoPersonas();
		Persona persona1 = new Persona("Pepe", 1, 2, 3, 4);
		grafo.aņadirPersona(persona1);
	}

	@Test
	public void agregarDosPersonasTest() 
	{
		GrafoPersonas grafo = new GrafoPersonas();
		Persona persona1 = new Persona("Pepe", 1, 2, 3, 4);
		Persona persona2 = new Persona("Raul", 3, 2, 1, 5); //|1-3|+|3-1|+|4-5| = 2+2+1 = 5.
		grafo.aņadirPersona(persona1);
		grafo.aņadirPersona(persona2);
		assertEquals(grafo.getAristas().get(0).getPeso(), 5);
	}
	
	@Test
	public void agregarTresPersonasTest() 
	{
		GrafoPersonas grafo = new GrafoPersonas();
		Persona persona1 = new Persona("Pepe", 1, 2, 3, 4);
		Persona persona2 = new Persona("Raul", 3, 2, 1, 5);
		Persona persona3 = new Persona("Rosa", 5, 5, 5, 5);
		grafo.aņadirPersona(persona1);
		grafo.aņadirPersona(persona2);
		grafo.aņadirPersona(persona3);
			
		assertEquals(grafo.getAristas().size(), 3);
	}
	
	
	@Test
	public void agregarCuatroPersonasTest() 
	{
		GrafoPersonas grafo = new GrafoPersonas();
		Persona persona1 = new Persona("Pepe", 1, 2, 3, 4);
		Persona persona2 = new Persona("Raul", 3, 2, 1, 5);
		Persona persona3 = new Persona("Rosa", 5, 5, 5, 5);
		Persona persona4 = new Persona("Luis", 4, 4, 4, 4);
		grafo.aņadirPersona(persona1);
		grafo.aņadirPersona(persona2);
		grafo.aņadirPersona(persona3);
		grafo.aņadirPersona(persona4);
			
		assertEquals(grafo.getAristas().size(), 6);
	}
	
	@Test 
	public void eliminarMayorPesoTest()
	{
		GrafoPersonas grafo = new GrafoPersonas();
		Persona persona1 = new Persona("Pepe", 1, 2, 3, 4); //(0, 1)= 5, (0, 2)= 10, (0, 3)= 6 
		Persona persona2 = new Persona("Raul", 3, 2, 1, 5); //(1, 2)= 9, (1, 3)= 7,  (2, 3)= 4
		Persona persona3 = new Persona("Rosa", 5, 5, 5, 5);
		Persona persona4 = new Persona("Luis", 4, 4, 4, 4);
		grafo.aņadirPersona(persona1);
		grafo.aņadirPersona(persona2);
		grafo.aņadirPersona(persona3);
		grafo.aņadirPersona(persona4);
			
		grafo.eliminarAristaMasPesada();
		
		assertFalse(grafo.getGrafo().existeArista(0, 2));	
	}
	
	@Test
	public void conseguirListasTest()
	{
		GrafoPersonas grafo = new GrafoPersonas();
		Persona persona1 = new Persona("Pepe", 1, 2, 3, 4); //(0, 1)= 5, (0, 2)= 10, (0, 3)= 6 
		Persona persona2 = new Persona("Raul", 3, 2, 1, 5); //(1, 2)= 9, (1, 3)= 7,  (2, 3)= 4
		Persona persona3 = new Persona("Rosa", 5, 5, 5, 5);
		Persona persona4 = new Persona("Luis", 4, 4, 4, 4);
		grafo.aņadirPersona(persona1);
		grafo.aņadirPersona(persona2);
		grafo.aņadirPersona(persona3);
		grafo.aņadirPersona(persona4);
	}
}
