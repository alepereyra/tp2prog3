package negocio.Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import negocio.Persona;

public class PersonaTest 
{
	@Test
	public void inicializarPersonaTest()
	{
		Persona persona = new Persona("Juan Luis Gutierrez", 1, 3, 4, 5);
		
		assertEquals(persona.getDeportes(), 1);
		assertEquals(persona.getCiencia(), 3);
		assertEquals(persona.getMusica(), 4);
		assertEquals(persona.getEspectaculo(), 5);
		assertEquals(persona.getNombre(), "Juan Luis Gutierrez");
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void inicializarPersonaNumeroMayorQue5Test()
	{
		@SuppressWarnings("unused")
		Persona persona = new Persona("Juan Luis Gutierrez", 1, 2, 3, 6);
	}

	@Test(expected = IllegalArgumentException.class)
	public void inicializarPersonaNumeroMenorQue0Test()
	{
		@SuppressWarnings("unused")
		Persona persona = new Persona("Juan Luis Gutierrez", 1, 0, 3, 5);
	}
	
	@Test
	public void indiceDeSimilaridadIgualTest()
	{
		Persona persona1 = new Persona ("Pepe", 2, 3, 4, 5);
		Persona persona2 = new Persona ("Ruben", 2, 3, 4, 5);
		
		assertEquals (0, Persona.similaridad(persona1, persona2));
	}
	
	@Test
	public void indiceDeSimilaridadDistintosTest()
	{
		Persona persona1 = new Persona ("Pepe", 2, 3, 2, 4); //|2-1|+|3-5|+|2-1|+|4-3| = 1+2+1+1 = 5
		Persona persona2 = new Persona ("Ruben", 1, 5, 1, 3);
		
		assertEquals(5, Persona.similaridad(persona1,  persona2));
	}
	
}
