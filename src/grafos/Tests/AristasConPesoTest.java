package grafos.Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import grafos.Grafo;

public class AristasConPesoTest 
{
	@Test
	public void consultarPesoTest() 
	{
		Grafo grafo = new Grafo (5);
		
		grafo.agregarArista(0, 2, 5);
		
		assertEquals(5, grafo.pesoArista(0, 2));
	}
	
	@Test
	public void consultarInvertidoPesoTest() 
	{
		Grafo grafo = new Grafo (5);
		
		grafo.agregarArista(0, 2, 5);
		
		assertEquals(5, grafo.pesoArista(2, 0));
	}
	
	@Test
	public void consultarPesoErroneoTest() 
	{
		Grafo grafo = new Grafo (5);
		
		grafo.agregarArista(0, 2, 6);
		
		assertNotEquals(5, grafo.pesoArista(0, 2));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void consultarPesoAristaInexistenteTest() 
	{
		Grafo grafo = new Grafo (5);
		
		grafo.agregarArista(0, 2, 5);
		
		grafo.pesoArista(0, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void eliminarAristaPreguntarPesoTest() 
	{
		Grafo grafo = new Grafo (5);
		
		grafo.agregarArista(0, 2, 5);
		grafo.eliminarArista(0, 2);
		
		grafo.pesoArista(0, 2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void eliminarAristaInversaTest() 
	{
		Grafo grafo = new Grafo (5);
		
		grafo.agregarArista(0, 2, 5);
		grafo.eliminarArista(2, 0);
		
		grafo.pesoArista(0, 2);
	}
	
}
