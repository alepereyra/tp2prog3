package grafos.Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import grafos.Grafo;

public class VerticesTest {

	@Test
	public void agregarVerticeUniversalTest()
	{
		Grafo grafo = new Grafo(4);
		
		grafo.agregarVerticeUniversal();
		
		int[] esperado = {0, 1, 2, 3};
		
		Asserts.iguales(esperado, grafo.vecinos(4));
	}
	
	@Test
	public void agregarVerticeTest()
	{
		Grafo grafo = new Grafo(4);
		
		grafo.agregarVertice();
		
		assertEquals(5, grafo.tamaņo());
	}

	@Test
	public void eliminarVerticeTamaņoTest()
	{
		Grafo grafo = new Grafo(5);
		
		grafo.conectarTodosLosVertices();
		
		grafo.eliminarVertice(1);
		
		assertEquals(4, grafo.tamaņo());
	}
	
	
	// Problematico!
	@Test
	public void eliminarVerticePunteroTest()
	{
		Grafo grafo = new Grafo(5);
		
		grafo.conectarTodosLosVertices();
		
		grafo.eliminarVertice(1);
		
		int[] esperado = {0, 3, 4};
		
		Asserts.iguales(esperado, grafo.vecinos(1));
	}
	
}
