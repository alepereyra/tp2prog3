package grafos.Tests;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

import grafos.BFS;
import grafos.Grafo;

public class BFSTest 
{
	@Test(expected = IllegalArgumentException.class)
	public void testNull() 
	{
		BFS.esConexo(null);
	}
	
	@Test
	public void alcanzablesTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		
		Set<Integer> alcanzables = BFS.alcanzables(grafo, 0);
		
		int[] esperados = {0, 1, 2};
		Asserts.iguales(esperados,alcanzables);
	}
	
	@Test
	public void conexoTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(2, 4);

		
		assertTrue( BFS.esConexo(grafo));
	}
	
	@Test
	public void noconexoTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(2, 3);
		
		assertFalse( BFS.esConexo(grafo));
	}
	
}
