package grafos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class BFS 
{
	private static ArrayList<Integer> lista;
	private static boolean[] marcados;
	
	// Inicializo la lista con el vertice de origen y el arreglo de booleanos que representan a los vertices ya verificados.
	private static void inicializar (Grafo grafo, int origen) 
	{
		lista = new ArrayList<Integer>();
		lista.add(origen);
		marcados = new boolean[grafo.tama�o()];
	}
	
	// Devuelvo si el grafo es conexo o no.
	public static boolean esConexo(Grafo grafo)
	{
		Verificadores.verificarNull(grafo);
		
		if( grafo.tama�o() == 0)
			return true;
		
		if (alcanzables(grafo, 0).size() == grafo.tama�o())
			return true;
		
		return false;
	}

	// Este metodo devuelve todos los vertices alcanzables por el vertice "origen".
	public static Set<Integer> alcanzables(Grafo grafo, int origen) {
		Set<Integer> ret = new HashSet<Integer>();
		inicializar (grafo, origen);
		
		while ( lista.size() > 0)
		{
			int i = lista.get(0);
			marcados[i] = true;
			ret.add(i);
			
			agregarVecinosPendientes(grafo, i);
			lista.remove(0);
		}
		
		return ret;
	}

	
	// Este metodo es auxiliar de alcanzables. Coloca en la lista a todos los vecinos del vertice indicado.
	private static void agregarVecinosPendientes(Grafo grafo, int i) {	
		for (int vertice : grafo.vecinos(i) )
		{
			if (marcados[vertice] == false && lista.contains(vertice) == false)
				lista.add(vertice);
		}
		
	}	
}
