package grafos;

public class Arista 
{

	// Representamos a las aristas con sus dos vertices y su peso.
	private int vertice1;
	private int vertice2;
	private int peso;
	
	
	// Constructor de la clase.
	public Arista (int vertice1, int vertice2, int peso)
	{
		this.vertice1 = vertice1;
		this.vertice2 = vertice2;
		this.peso = peso;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + vertice1;
		result = prime * result + vertice2;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arista other = (Arista) obj;
		if (vertice1 != other.vertice1)
			return false;
		if (vertice2 != other.vertice2)
			return false;
		return true;
	}
	
	// toString, devuelve un String del tipo "(vertice1, vertice2, peso)"
	public String toString() 
	{
		String ret = "(" + vertice1 + ", " + vertice2 + ", " + peso + ")";
		return ret;
	}
	
	// Getters y setters.
	public int getVertice1() {
		return vertice1;
	}

	public int getVertice2() {
		return vertice2;
	}

	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}
	
}
