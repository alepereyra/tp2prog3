package grafos;

public class Verificadores 
{

	// Verifica que no se introduzcan loops
	public static void verificarDistintos(int i, int j) {
			if( i == j)
				throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")." );
	}
	
	// Verifica que sea un vertice valido
	public static void verificarVertice(int i, Grafo grafo) {
			if( i < 0)
				throw new IllegalArgumentException("El vertice no puede ser negativo: " + i + ".");
			
			if( i >= grafo.tamaño())
				throw new IllegalArgumentException("Los vertices deben estar entre 0 y |v|-1: " + i + ".");
	}
	
	// Verifica que la cantidad de vertices sea >= 0
	public static void verificarTamañoNegativo (int i)
	{
		if (i < 0)
			throw new IllegalArgumentException("Se esta intentando crear un grafo con !V! < 0: " + i + ".");
	}
	
	// Este método verifica que no se haya ingresado un grafo null.
		public static void verificarNull(Grafo grafo)
		{
			if (grafo == null)
				throw new IllegalArgumentException("Se intento consultar un grafo no inicializado");
		}
		
	// Verifica que los numeros esten en rango.
	public static void verificarNumeros(int a, int b, int c, int d)
	{
		int[] verificar = {a, b, c, d};
		for (int j : verificar)
		{
			if (j < 1 || j > 5)
				throw new IllegalArgumentException ("El valor de interes tiene que estar entre 1 y 5: " + j + ".");			
		}
	}	
	
}
