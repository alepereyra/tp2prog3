package grafos;

import java.util.ArrayList;

public class ArbolGeneradorMinimo 
{
	static int [] unionFind;
	static ArrayList<Arista> lista;

	// Inicializo las listas auxiliares del metodo algoritmoDeKruslal.
	private static void inicializar(Grafo grafo)
	{
		lista = new ArrayList<Arista>();
		for (Arista arista : grafo.getAristas())
			lista.add(arista);
		unionFind = new int[grafo.tama�o()];
		for (int i = 0; i < grafo.tama�o(); i++)
			unionFind[i] = i;

	}
	
	// Este metodo devuelve un AGM del grafo recibido.
	static public Grafo algoritmoDeKruskal(Grafo grafo)
	{
		Verificadores.verificarNull(grafo);
		if (!BFS.esConexo(grafo))
			throw new IllegalArgumentException("El grafo ingresado no es conexo");
		Grafo ret = new Grafo(grafo.tama�o());
		inicializar(grafo);
		
		while(ret.getAristas().size() < grafo.tama�o() - 1)
		{
			Arista auxiliar = elegirArista();
			ret.agregarArista(auxiliar.getVertice1(),auxiliar.getVertice2(), auxiliar.getPeso());
			union(auxiliar.getVertice1(), auxiliar.getVertice2());
		}
		return ret;
		
	}
	
	// Este m�todo elige la arista idonea para el Arbol generador minimo.
	private static Arista elegirArista()
	{
		boolean flag = false;
		Arista ret = lista.get(0);
		
		while (flag == false)
		{
			ret = obtenerMenorPeso(lista);
			if (find (ret.getVertice1(), ret.getVertice2()))
			{
				lista.remove(ret);
			} 
			else
			{
				flag = true;
				lista.remove(ret);
			}		
		}
		return ret;
	}
	
	// Devuelve la arista con menor peso entre las disponibles 
	private static Arista obtenerMenorPeso(ArrayList<Arista> aristas)
	{
		Arista ret = aristas.get(0);
		for (Arista arista : aristas)
				{
					if (arista.getPeso() < ret.getPeso())
						ret = arista;
				}
		return ret;
	}
	
	// Devuelve la raiz del arbol al que pertenece el vertice especificado
	private static int root (int i)
	{
		while (unionFind[i] != i)
			i = unionFind[i];
		return i;
	}
	
	// Devuelve si los dos vertices especificados pertenecen a la misma componente conexa
	private static boolean find (int i, int j)
	{
		return root(i) == root (j);
	}
	
	// Une dos componentes conexas distintas
	private static void union (int i, int j)
	{
		int ri = root(i);
		int rj = root(j);
		unionFind[ri] = rj;
	}
}
