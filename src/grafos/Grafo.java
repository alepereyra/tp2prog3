package grafos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Grafo 
{

	// Representamos el grafo mediante lista de vecinos
	// Guardamos todas las aristas en una lista para almacenar informacion como el peso
	private ArrayList<HashSet<Integer>> vecinos;
	private ArrayList<Arista> aristas;

	
	// La cantidad de vertices esta predeterminada desde el constructor
	public Grafo (int vertices)
	{
		Verificadores.verificarTama�oNegativo(vertices);
		
		vecinos = new ArrayList<HashSet<Integer>>();
		for ( int i = 0; i < vertices; i++)
			vecinos.add(new HashSet<Integer>());
		
		this.aristas = new ArrayList<Arista>();

	}
	
	// Agregado de aristas con peso definido
	public void agregarArista (int i, int j, int peso)
	{
		Verificadores.verificarVertice(i, this);
		Verificadores.verificarVertice(j, this);
		Verificadores.verificarDistintos(i, j);
		
		vecinos.get(i).add(j);
		vecinos.get(j).add(i);
		
		aristas.add(new Arista(i, j, peso));	
	}
	
	// Agregado de aristas sin peso definido. Se agrega con peso "0" por defecto.
	public void agregarArista (int i, int j)
	{
		Verificadores.verificarVertice(i, this);
		Verificadores.verificarVertice(j, this);
		Verificadores.verificarDistintos(i, j);
		
		vecinos.get(i).add(j);
		vecinos.get(j).add(i);
		
		Arista aux = new Arista(i, j, 0);
		aristas.add(aux);
	}
	
	// Agregado de vertices
	public void agregarVertice()
	{
		this.vecinos.add(new HashSet<Integer>());
	}
	
	// Agregado de vertices universales (que estan conectados con todos los otros vertices)
	public void agregarVerticeUniversal()
	{
		this.vecinos.add(new HashSet<Integer>());
		
		for (int i = 0; i < this.tama�o() - 1; i++) 
		{
			this.agregarArista(i, this.tama�o()-1);
		}	
	}
	
	// Metodo que recibe la posicion de un vertice y lo elimina 
	// (elimina tambien todas las aristas relacionadas con el vertice). Problematico!!!
	public void eliminarVertice(int i)
	{	
		for (int j = 0; j < this.tama�o(); j++)
		{
			if (this.vecinos(j).contains(i))
				this.eliminarArista(i, j);
		}
		this.vecinos.remove(i);
	}
	
	// Metodo que convierte al grafo en un grafo completo
	public void conectarTodosLosVertices()
	{
		for (int i = 0; i < this.tama�o(); i++)
		{
			for (int j = 0; j < this.tama�o(); j++) if( i != j)
			{			
				agregarArista (i, j);
			}
		}
	}
	
	// Eliminado de aristas
	public void eliminarArista (int i, int j)
		{
			Verificadores.verificarVertice(i, this);
			Verificadores.verificarVertice(j, this);
			Verificadores.verificarDistintos(i, j);
			
			vecinos.get(i).remove(j);
			vecinos.get(j).remove(i);
			
			for(int k = 0; k < aristas.size(); k++)
			{
				if ((aristas.get(k).getVertice1() == i || aristas.get(k).getVertice1() == j) &&
						(aristas.get(k).getVertice2() == i || aristas.get(k).getVertice2() == j))
					aristas.remove(k);
			}
		}
	
	// Informo si existe la arista especificada
	public boolean existeArista (int i, int j)
	{
		Verificadores.verificarVertice(i, this);
		Verificadores.verificarVertice(j, this);
		
		return vecinos.get(i).contains(j);
	}
	
	// Cantidad de vertices
	public int tama�o ()
	{
		return vecinos.size();
	}
	
	// Devuelvo la lista que contiene a todos los vecinos del vertice especificado
	public Set<Integer> vecinos (int i)
	{
		Verificadores.verificarVertice(i, this);
		
		return vecinos.get(i);
	}
	
	// Devuelvo el peso (o coste) de la arista que conecta a los vertices i y j.
	public int pesoArista (int i, int j)
	{
		for(int k = 0; k < aristas.size(); k++)
		{
			if ((aristas.get(k).getVertice1() == i || aristas.get(k).getVertice1() == j) &&
					(aristas.get(k).getVertice2() == i || aristas.get(k).getVertice2() == j))
				return aristas.get(k).getPeso() ;
		}
		throw new IllegalArgumentException("La arista (" + i + ", " + j + ") no existe.");
	}
	
	public int pesoTotal()
	{
		int ret = 0;
		for (Arista arista : aristas)
			ret += arista.getPeso();
		return ret;
	}
	
	
	public ArrayList<Arista> getAristas()
	{
		return this.aristas;
	}
	
	public ArrayList<HashSet<Integer>> getVecinos()
	{
		return this.vecinos;
	}
}
